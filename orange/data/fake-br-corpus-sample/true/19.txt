Laudo declara que Paulo Maluf pode continuar no PresÃ­dio da Papuda

IML confirmou que Maluf tem cÃ¢ncer de prÃ³stata e problema de coluna. Mas que, apesar de doente, pode continuar preso na PenitenciÃ¡ria da Papuda

Laudo declara que Paulo Maluf pode continuar no PresÃ­dio da Papuda
IML confirmou que Maluf tem cÃ¢ncer de prÃ³stata e problema de coluna. Mas que, apesar de doente, pode continuar preso na PenitenciÃ¡ria da Papuda.
FACEBOOK
Um laudo de dois legistas declarou que o deputado Paulo Maluf pode continuar no PresÃ­dio da Papuda.

O deputado federal Paulo Maluf, do Progressistas, foi examinado no dia 22 de dezembro, assim que chegou vindo de SÃ£o Paulo. Os mÃ©dicos do Instituto MÃ©dico Legal avaliaram a saÃºde dele a pedido do juiz da vara de execuÃ§Ãµes penais de BrasÃ­lia, Bruno Macacari.

Os advogados de defesa queriam que Maluf fosse transferido para prisÃ£o domiciliar alegando que ele estÃ¡ doente e tem uma idade avanÃ§ada, 86 anos.

A conclusÃ£o do laudo do IML confirmou que Maluf tem cÃ¢ncer de prÃ³stata e problema de coluna. Mas que, apesar de doente, pode continuar preso na PenitenciÃ¡ria da Papuda.

Ao responder a perguntas especÃ­ficas do juiz, os mÃ©dicos do IML dizem que Paulo Maluf tem doenÃ§as graves e permanentes, mas "no momento" nÃ£o apresenta grave "limitaÃ§Ã£o de atividade e restriÃ§Ã£o de participaÃ§Ã£o". Ao final, o juiz pergunta se o sentenciado exige cuidados contÃ­nuos que nÃ£o possam ser prestados na PenitenciÃ¡ria da Papuda. E a resposta Ã©: "NÃ£o. Todavia deverÃ¡ ter acompanhamento ambulatorial especializado".

Sobre o cÃ¢ncer, os peritos disseram que ele estÃ¡ sem sintomas, mas que o prognÃ³stico futuro exige a avaliaÃ§Ã£o de um especialista em oncologia.

O juiz ainda nÃ£o decidiu sobre o pedido de prisÃ£o domiciliar para Paulo Maluf.

O juiz Bruno Macacari nÃ£o permitiu que o mÃ©dico particular de Paulo Maluf examinasse o deputado na cadeia. O advogado de Maluf disse que essa negativa Ã© incompreensÃ­vel e atenta contra o direito de defesa. Que o laudo confirma os problemas de saÃºde do deputado, mas nÃ£o analisou o problema cardÃ­aco dele. E que a negativa de prisÃ£o domiciliar vai prejudicar a saÃºde do cliente.